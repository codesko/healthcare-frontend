import React, { useEffect } from 'react';
import axios from 'axios';
import Search from '../../domains/search/Search';
import SearchFilters from '../../domains/search/filter/SearchFilters';
import SearchResults from '../../domains/search/result/SearchResults';
import { GET_PROVIDERS } from './api';
import { useProviders } from '../../hooks/useProviders';
import { useSearch, useSearchFilterButtonToggle } from '../../hooks/useSearch';
import { usePagination } from '../../hooks/usePagination';

const ProvidersContainer = () => {
    const { providers, setProviders, setStatus, status } = useProviders();
    const {
        searchValue,
        setSearchValue,
        setSearchFilter,
        searchFilters,
    } = useSearch();
    const {
        isFilterButtonClicked,
        toggleFilterButton,
    } = useSearchFilterButtonToggle();
    const api = GET_PROVIDERS;
    const {
        limit,
        currentPage,
        goNextPage,
        goToPage,
        goPreviousPage,
    } = usePagination();

    useEffect(() => {
        setStatus('loading');

        const getProviders = async () => {
            try {
                const response = await axios.get(api, {
                    params: {
                        ...searchFilters.reduce((acc: any, val: any) => {
                            acc[val.name] = val.value;
                            return acc;
                        }, {}),
                        page: currentPage,
                        name: searchValue,
                        limit,
                    },
                });

                if (response.status === 200) {
                    setProviders(response.data);
                    setTimeout(() => setStatus('loaded'), 1000);
                }
            } catch (error) {
                setProviders({ items: [] });
                setStatus('error');
            }
        };

        getProviders();
    }, [
        searchValue,
        searchFilters,
        currentPage,
        api,
        limit,
        setStatus,
        setProviders,
    ]);

    return (
        <>
            <Search
                setValue={(value: string) => setSearchValue(value)}
                isFilterButtonClicked={isFilterButtonClicked}
                toggleFilterButton={() => toggleFilterButton()}
            />
            {isFilterButtonClicked && (
                <SearchFilters
                    filters={searchFilters}
                    setFilter={(name: string, value: string) =>
                        setSearchFilter(name, value)
                    }
                />
            )}
            <SearchResults
                data={providers}
                status={status}
                currentPage={currentPage}
                goNextPage={() => goNextPage()}
                goPreviousPage={() => goPreviousPage()}
                goToPage={(to: number) => goToPage(to)}
                limit={limit}
            />
        </>
    );
};

export default ProvidersContainer;
