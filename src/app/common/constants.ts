export const theme = {
    colors: {
        GREY_PRIMARY: '#f7f7f7',
        GREY_HOVER: '#ececec',
        RED_PRIMARY: '#bb4141',
        BLUE_PRIMARY: '#a2cde8',
    },
};
