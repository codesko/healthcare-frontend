import React from 'react';
import { Switch, Route } from 'react-router-dom';
import ProvidersContainer from '../../scenes/providers/ProvidersContainer';

const Routes = () => {
    return (
        <Switch>
            <Route exact path="/" component={ProvidersContainer} />
        </Switch>
    );
};

export default Routes;
