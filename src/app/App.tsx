import React from 'react';
import MainLayout from './layout/MainLayout';
import { BrowserRouter as Router } from 'react-router-dom';
import { ThemeProvider } from 'styled-components';
import { theme } from './common/constants';
import { createGlobalStyle } from 'styled-components';

const GlobalStyleInjector = createGlobalStyle`
    body {
        margin: 0;
        font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', 'Roboto',
            'Oxygen', 'Ubuntu', 'Cantarell', 'Fira Sans', 'Droid Sans',
            'Helvetica Neue', sans-serif;
        -webkit-font-smoothing: antialiased;
        -moz-osx-font-smoothing: grayscale;
    }

    *:focus {
        outline: none;
    }
`;

const App = () => {
    return (
        <Router>
            <GlobalStyleInjector />
            <ThemeProvider theme={theme}>
                <MainLayout />
            </ThemeProvider>
        </Router>
    );
};

export default App;
