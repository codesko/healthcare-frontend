import React from 'react';
import styled from 'styled-components';
import Sidebar from '../../domains/sidebar/Sidebar';
import Routes from '../router/Routes';

const Container = styled.div`
    width: 100%;
    display: flex;
    min-height: 100vh;
`;
const Main = styled.main`
    width: 100%;
    min-height: 100vh;
    background: white;
`;

const MainLayout = () => {
    return (
        <Container>
            <Sidebar />
            <Main>
                <Routes />
            </Main>
        </Container>
    );
};

export default MainLayout;
