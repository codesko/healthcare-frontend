import { useState } from 'react';
import { useImmer } from 'use-immer';

export interface Filter {
    name?: string;
    value?: string;
}

export const useSearch = () => {
    const defaultFilters: Filter[] = [];
    const defaultValue: string = '';

    const [searchValue, setSearchValue] = useState(defaultValue);
    const [searchFilters, updateFilters] = useImmer(defaultFilters);

    const setSearchFilter = (name: string, value: string) => {
        updateFilters(draft => {
            draft.filter(item => item.name !== name);
            draft.push({ name, value });
        });
    };

    return {
        searchFilters,
        setSearchFilter,
        searchValue,
        setSearchValue,
    };
};

export const useSearchFilterButtonToggle = () => {
    const [isFilterButtonClicked, set] = useImmer(false);
    const toggleFilterButton = () => set(draft => !draft);

    return {
        isFilterButtonClicked,
        toggleFilterButton,
    };
};
