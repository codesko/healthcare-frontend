import { useState } from 'react';

export const usePagination = () => {
    const defaultPage: number = 1;
    const defaultLimit: number = 25;
    const [currentPage, setCurrentPage] = useState(defaultPage);
    const [limit, setLimit] = useState(defaultLimit);

    const goNextPage = () => {
        setCurrentPage(currentPage + 1);
    };

    const goPreviousPage = () => {
        setCurrentPage(currentPage - 1);
    };

    const goToPage = (to: number) => {
        setCurrentPage(to);
    };

    return {
        limit,
        goNextPage,
        goPreviousPage,
        goToPage,
        currentPage,
    };
};
