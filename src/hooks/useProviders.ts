import { useState } from 'react';

export interface Provider {
    drg_name: string;
    name: string;
    street: string;
    city: string;
    state: string;
    zip_code: string;
    hospital_referral_region_description: string;
    total_discharges: string;
    avg_covered_charges: string;
    avg_total_payments: string;
    avg_medicare_payments: string;
}

export interface Providers {
    items: Provider[];
    itemCount?: number;
    pageCount?: number;
    totalItems?: number;
    next?: string;
    previous?: string;
}

export const useProviders = () => {
    const defaultProviders: Providers = {
        items: [],
    };
    const [providers, setProviders] = useState(defaultProviders);
    const [status, setStatus] = useState('loading');

    return {
        status,
        setStatus,
        providers,
        setProviders,
    };
};
