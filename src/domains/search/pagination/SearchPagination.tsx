import React from 'react';
import styled from 'styled-components';

const Wrapper = styled.div`
    text-align: center;
    margin: 2em;
`;

const SearchPaginationItem = styled.a`
    margin: 0 1em;
    text-decoration: none;

    ${(props: { active?: boolean; theme: any }) =>
        props.active
            ? `
            color: black;
            font-weight: 500;
        `
            : `
            color: #ddd

            &:hover {
                cursor: pointer;
                font-weight: 500;
                color: #6b6b6b
            }
        `}
`;

interface Props {
    currentPage: number;
    previous?: string;
    next?: string;
    pageCount?: number;
    goNextPage: () => void;
    goPreviousPage: () => void;
    goToPage: (to: number) => void;
    limit: number;
}

const buildRange = (
    current: number,
    { min = 1, total = 25, length = 5 } = {}
) => {
    if (length > total) length = total;

    let start = current - Math.floor(length / 2);
    start = Math.max(start, min);
    start = Math.min(start, min + total - length);

    return Array.from({ length }, (el, i) => start + i);
};

const SearchPagination = ({
    currentPage,
    previous,
    next,
    goPreviousPage,
    goNextPage,
    goToPage,
}: Props) => {
    return (
        <Wrapper>
            {previous !== '' && (
                <SearchPaginationItem onClick={() => goPreviousPage()}>
                    ← Previous
                </SearchPaginationItem>
            )}

            {buildRange(currentPage).map(item => (
                <SearchPaginationItem
                    active={item === currentPage}
                    key={`pagination_item_${item}`}
                    onClick={() => goToPage(item)}
                >
                    {item}
                </SearchPaginationItem>
            ))}

            {next !== '' && (
                <SearchPaginationItem onClick={() => goNextPage()}>
                    Next →
                </SearchPaginationItem>
            )}
        </Wrapper>
    );
};

export default SearchPagination;
