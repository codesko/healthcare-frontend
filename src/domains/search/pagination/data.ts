export const pagination = [
    {
        page: 1,
        active: true,
        href: '#',
    },
    {
        page: 2,
        active: false,
        href: '#',
    },
    {
        page: 3,
        active: false,
        href: '#',
    },
    {
        page: 4,
        active: false,
        href: '#',
    },
];
