import React, { useState } from 'react';
import styled from 'styled-components';
import { Filter } from '../../../hooks/useSearch';

const Wrapper = styled.div`
    display: grid;
    grid-template-columns: auto auto auto;
    grid-gap: 1em;
    padding: 1em;
    background-color: #fdfdfd;
`;

const SearchFiltersInput = styled.input`
    display: block;
    font-size: 1em;
    height: 2em;
    border: none;
    border-bottom: 1px solid #eee;
    background: transparent;
    width: 100%;
    padding: 1em 0;

    ${(props: { uppercase?: boolean; theme: any }) =>
        props.uppercase &&
        `
            text-transform: uppercase
        `}

    &:focus {
        border-bottom: 1px solid #6b6b6b;
    }
`;

const SearchFiltersLabel = styled.label`
    text-transform: uppercase;
    font-weight: 500
    font-size: .7em
`;

interface Props {
    setFilter: (name: string, value: string) => void;
    filters: Filter[];
}

const filtersList = [
    {
        name: 'max_discharges',
        desc: 'The maximum number of Total Discharges',
        type: 'number',
    },
    {
        name: 'min_discharges',
        desc: 'The minimum number of Total Discharges',
        type: 'number',
    },
    {
        name: 'max_average_covered_charges',
        desc: 'The maximum Average Covered Charges',
        type: 'number',
    },
    {
        name: 'min_average_covered_charges',
        desc: 'The minimum Average Covered Charges',
        type: 'number',
    },
    {
        name: 'max_average_medicare_payments',
        desc: 'The maximum Average Medicare Payment',
        type: 'number',
    },
    {
        name: 'min_average_medicare_payments',
        desc: 'The minimum Average Medicare Payment',
        type: 'number',
    },
    {
        name: 'state',
        desc: 'The exact state that the provider is from',
        type: 'string',
    },
];

const SearchFilters = ({ setFilter, filters }: Props) => {
    const [time, setTime] = useState(0);

    const onKeyPress = (name: string, event: any) => {
        const value = event.target.value;
        clearTimeout(time);

        setTime(
            setTimeout(() => {
                setFilter(name, value);
            }, 1000)
        );
    };

    const findByNameAndGetValue = (name: string) => {
        const filter = filters.find(item => item.name === name);

        if (filter !== undefined) {
            return filter.value;
        }
    };

    return (
        <Wrapper>
            {filtersList.map(
                (item: { name: string; desc: string; type: string }) => (
                    <SearchFiltersLabel key={item.name}>
                        {item.desc}
                        <SearchFiltersInput
                            placeholder={item.name}
                            min={0}
                            type={item.type}
                            uppercase={item.type === 'string' && true}
                            defaultValue={findByNameAndGetValue(item.name)}
                            onChange={(event: any) =>
                                onKeyPress(item.name, event)
                            }
                        />
                    </SearchFiltersLabel>
                )
            )}
        </Wrapper>
    );
};

export default SearchFilters;
