import React from 'react';
import styled from 'styled-components';
import {
    Table as T,
    Thead,
    Tbody,
    Tr,
    Th,
    Td,
} from 'react-super-responsive-table';
import 'react-super-responsive-table/dist/SuperResponsiveTableStyle.css';
import SearchPagination from '../pagination/SearchPagination';
import { Providers } from '../../../hooks/useProviders';

const Wrapper = styled.div`
    margin: 2em;
`;

const ResultsInfo = styled.div`
    display: flex;
    align-items: center;
    justify-content: space-between;
`;

const TableWrapper = styled.div`
    overflow: auto;
`;

const Table = styled(T)`
    width: 100%;
    border-spacing: 0;
`;

const TableHead = styled(Thead)`
    background: #f7f7f7;
`;

const TableBody = styled(Tbody)``;

const TableTh = styled(Th)`
    font-size: 0.7em;
    text-transform: uppercase;
    padding: 0.7em;
    color: #6b6b6b;
`;

const TableTd = styled(Td)`
    padding: 1em 0.5em;
    color: black;
    border-bottom: 1px solid #f7f7f7;
`;

const TableTr = styled(Tr)`
    @media screen and (max-width: 40em) {
        padding: 1em 0.25em !important;
    }
`;

const Loading = styled.div`
    margin: 1em;
    font-weight: bold;
    color: ${props => props.theme.colors.RED_PRIMARY}
    font-size: 6em;
    text-align: center;
    transform: scale(1);
    animation: pulse 2s infinite;

    @keyframes pulse {
        0% {
            transform: scale(0.5);
        }

        70% {
            transform: scale(1);
        }

        100% {
            transform: scale(0.5);
        }
    }
`;

const Error = styled.div`
    margin: 3em;
    font-weight: bold;
    font-size: 1.5em;
    text-align: center;
`;

const Empty = styled.div`
    margin: 3em;
    font-weight: bold;
    font-size: 1.5em;
    text-align: center;
`;

const tableHeaders = [
    'DRG',
    'Name',
    'Street',
    'City',
    'State',
    'Zip Code',
    'Region Description',
    'Total Discharges',
    'Avg Covered Charges',
    'Avg Total Payments',
    'Avg Medicare Payments',
];

interface Props {
    status: string;
    data: Providers;
    currentPage: number;
    goNextPage: () => void;
    goPreviousPage: () => void;
    goToPage: (to: number) => void;
    limit: number;
}

const SearchResults = ({
    data,
    status,
    goPreviousPage,
    goNextPage,
    goToPage,
    currentPage,
    limit,
}: Props) => {
    switch (status) {
        case 'error':
            return <Error>An error occurred! Please try again later.</Error>;

        case 'loaded':
            if (data.items.length === 0) {
                return (
                    <Empty>Nothing found. Try with different criteria.</Empty>
                );
            } else {
                return (
                    <Wrapper>
                        <ResultsInfo>
                            <h3>Results</h3>
                            <h4>{`${data.itemCount} / ${data.totalItems}`}</h4>
                        </ResultsInfo>
                        <TableWrapper>
                            <Table>
                                <TableHead>
                                    <TableTr>
                                        {tableHeaders.map((item, index) => (
                                            <TableTh key={`table_th_${index}`}>
                                                {item}
                                            </TableTh>
                                        ))}
                                    </TableTr>
                                </TableHead>
                                <TableBody>
                                    {data.items.map(
                                        (items: any, itemsIndex: number) => (
                                            <TableTr
                                                key={`table_tr_${itemsIndex}`}
                                            >
                                                {Object.values(items).map(
                                                    (
                                                        item: any,
                                                        itemIndex: number
                                                    ) => (
                                                        <TableTd
                                                            key={`table_td_${itemsIndex}_${itemIndex}`}
                                                        >
                                                            {item}
                                                        </TableTd>
                                                    )
                                                )}
                                            </TableTr>
                                        )
                                    )}
                                </TableBody>
                            </Table>
                        </TableWrapper>
                        <SearchPagination
                            currentPage={currentPage}
                            next={data.next}
                            previous={data.previous}
                            pageCount={data.pageCount}
                            goNextPage={goNextPage}
                            goPreviousPage={goPreviousPage}
                            goToPage={goToPage}
                            limit={limit}
                        />
                    </Wrapper>
                );
            }

        default:
            return (
                <Wrapper>
                    <Loading>❤</Loading>
                </Wrapper>
            );
    }
};

export default SearchResults;
