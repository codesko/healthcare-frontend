import React, { useState } from 'react';
import styled from 'styled-components';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faSearch } from '@fortawesome/free-solid-svg-icons';

const Wrapper = styled.div`
    width: 100%;
    display: flex;
    align-items: center;
    border-bottom: 1px solid ${props => props.theme.colors.GREY_PRIMARY};

    svg {
        position: relative;
        left: 1.5em;
        color: #808080;
    }
`;

const SearchInput = styled.input`
    color: #e1e1e1;
    height: 4.5em;
    color: #6b6b6b;
    padding: 0 3em;
    border: none;
    font-size: 1em;
    width: 100%;
`;

const SearchFilterButton = styled.button`
    border-radius: 30px;
    background: ${props => props.theme.colors.BLUE_PRIMARY};
    text-align: center;
    padding: 0.8em 1.6em;
    font-size: 1em;
    color: white;
    border: none;
    margin-right: 1em;
    white-space: nowrap;

    ${(props: { active: boolean }) =>
        props.active && `backgroud: red !important`}
`;

interface Props {
    setValue: (value: string) => void;
    toggleFilterButton: () => void;
    isFilterButtonClicked: boolean;
}

const Search = ({
    setValue,
    toggleFilterButton,
    isFilterButtonClicked,
}: Props) => {
    const [time, setTime] = useState(0);

    const onKeyPress = (event: any) => {
        const value = event.target.value;
        clearTimeout(time);

        if (value.length >= 3 || value.length === 0) {
            setTime(
                setTimeout(() => {
                    setValue(value);
                }, 1000)
            );
        }
    };

    return (
        <Wrapper>
            <FontAwesomeIcon icon={faSearch} />
            <SearchInput
                placeholder="Search..."
                onKeyDown={(event: any) => onKeyPress(event)}
            />
            <SearchFilterButton
                active={isFilterButtonClicked}
                onClick={toggleFilterButton}
            >
                {isFilterButtonClicked ? 'Filter ↑' : 'Filter ↓'}
            </SearchFilterButton>
        </Wrapper>
    );
};

export default Search;
