import React from 'react';
import styled from 'styled-components';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { items } from './data';

const Wrapper = styled.aside`
    min-width: 250px;
    min-height: 100vh;
    width: 250px;
    background: ${props => props.theme.colors.GREY_PRIMARY};

    @media screen and (max-width: 40em) {
        display: none;
    }
`;

const SidebarItems = styled.ul`
    padding: 0;
    margin: 0;
    list-style: none;
    width: 100%;
`;

const SidebarItem = styled.li`
    margin: 0.7em 0 0.7em 1em;
    padding: 15px 0 15px 20px;

    &:hover {
        background: ${props => props.theme.colors.GREY_HOVER};
        border-radius: 30px 0 0 30px;
        border-right: 2px solid ${props => props.theme.colors.BLUE_PRIMARY};
        transition: background 0.3s ease;
    }
`;

const SidebarItemLink = styled.a`
    color: #b1b1b1;
    text-decoration: none;
    font-weight: 300;

    &:hover {
        color: #6b6b6b;
        font-weight: 400;
    }
`;

const SidebarItemText = styled.span`
    margin-left: 0.5em;
`;

const Logo = styled.div`
    text-align: center;
    margin: 20px 0 50px 0;
`;

const LogoText = styled.span`
    font-weight: 900;
    color: ${props => props.theme.colors.RED_PRIMARY};
    font-size: 1.5em;
`;

const Sidebar = () => {
    return (
        <Wrapper>
            <Logo>
                <LogoText>❤ Healthcare</LogoText>
            </Logo>
            <SidebarItems>
                {items.map((item, index) => (
                    <SidebarItemLink
                        href={item.href}
                        key={`sidebar_item_${index}`}
                    >
                        <SidebarItem>
                            <FontAwesomeIcon icon={item.icon} />
                            <SidebarItemText>{item.name}</SidebarItemText>
                        </SidebarItem>
                    </SidebarItemLink>
                ))}
            </SidebarItems>
        </Wrapper>
    );
};

export default Sidebar;
