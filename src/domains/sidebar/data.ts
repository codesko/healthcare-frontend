import { faHospital, faSignOutAlt } from '@fortawesome/free-solid-svg-icons';

export const items = [
    {
        name: 'Providers',
        href: '#',
        icon: faHospital,
    },
    {
        name: 'Logout',
        href: '#',
        icon: faSignOutAlt,
    },
];
